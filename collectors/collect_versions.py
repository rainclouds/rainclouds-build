import sys
import json

from collectors.scrapers.gnu_scraper import scrape_gnu

scrapers = {
    "gnu": scrape_gnu
}


def get_version(dependency):
    if 'version_url' in dependency.keys():
        return scrapers[dependency['scraper']](dependency['name'], dependency['version_url'])
    else:
        return scrapers[dependency['scraper']](dependency['name'], None)


def get_download_url(dependency, version_string, version):
    return dependency['download_url'].format(
        version=version_string,
        major=version.major,
        minor=version.minor,
        micro=version.micro,
        post=version.post
    )


def main(dependency_json):
    versioned_dependencies = {}
    with open(dependency_json, 'r') as f:
        dependencies = json.load(f)

        for dependency in dependencies:
            if "scraper" not in dependency.keys():
                continue
            version_string, version = get_version(dependency)
            download_url = get_download_url(dependency, version_string, version)
            versioned_dependencies[dependency['name']] = {
                "version": version_string,
                "vcs-engine": dependency['vcs_engine'],
                "repository": download_url,
                "moves": [["{0}*".format(dependency['name']), "{0}".format(dependency['name'])]]
            }
    with open("out.json", "w") as outfile:
        toolchain = {
            "version": "23.10.0-alpha",
            "vendor": "rainclouds",
            "architecture": "x86_64",
            "make-flags": ["-j8"],
            "sources": versioned_dependencies
        }
        outfile.write(json.dumps(toolchain, indent=4))


if __name__ == "__main__":
    [_pyfile, _dependency_file] = sys.argv
    main(_dependency_file)
