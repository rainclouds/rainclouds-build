import requests
from bs4 import BeautifulSoup
from packaging import version
from packaging.version import Version


def scrape_gnu(dependency, url) -> (str, Version):
    url = "https://ftp.gnu.org/gnu/{0}".format(dependency)
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html5lib')
    rows = soup.find("table").find("tbody").find_all("tr")

    latest_post_release, post_string = None, None
    latest_pre_release, pre_string = None, None
    latest_dev_release, dev_string = None, None
    version_string, latest_version = None, None
    for row in rows:
        cells = row.find_all("td")
        if len(cells) == 0:
            continue
        filename = cells[1].get_text()
        if ".asc" not in filename and ".sig" not in filename and (".tar.gz" in filename or ".tar.xz" in filename):
            try:
                current_version = "-".join(filename.replace(".tar.xz", "").replace(".tar.gz", "").split("-")[1:])
                v = version.parse(current_version)
                if v.is_prerelease and (latest_pre_release is None or latest_pre_release < v):
                    latest_pre_release = v
                    pre_string = current_version
                elif v.is_devrelease and (latest_dev_release is None or latest_dev_release < v):
                    latest_dev_release = v
                    dev_string = current_version
                elif v.is_postrelease and (latest_post_release is None or latest_post_release < v):
                    latest_post_release = v
                    post_string = current_version
                elif latest_version is None or latest_version < v:
                    latest_version = v
                    version_string = current_version
            except Exception as e:
                continue
    return version_string, latest_version
