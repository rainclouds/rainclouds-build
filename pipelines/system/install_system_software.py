import sys

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain

# Buildstep functions
from pipelines.system.buildsteps.toolchain.build_binutils import build_binutils
from pipelines.system.buildsteps.toolchain.build_gcc import build_gcc
from pipelines.system.buildsteps.toolchain.build_linux_api_headers import build_linux_api_headers
from pipelines.system.buildsteps.toolchain.build_glibc import build_glibc
from pipelines.system.buildsteps.toolchain.test_glibc_toolchain import test_glibc_toolchain
from pipelines.system.buildsteps.toolchain.build_libstdcpp import build_libstdcpp
from pipelines.system.buildsteps.temptools.build_m4 import build_m4
from pipelines.system.buildsteps.toolchain.build_coreutils import build_coreutils
from pipelines.system.buildsteps.toolchain.build_bash import build_bash


def install_system_software(toolchain):
    print("[Install System Software] Installing base system software...")

    print("[Install System Software] Base system software has been successfully installed...")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    install_system_software(load_toolchain(_toolchain_file))
