import sys

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain


def build_cloud_system(toolchain):
    print("[Build Cloud System] Building the rainclouds filesystem structure.")

    # todo: Build cloud server filesystem structure (/store)
    # todo: Install mongodb
    # todo: Setup mongodb to run by default
    # todo: inquiry for default mongodb password
    # todo: Setup system mongodb database
    # todo: Bind virtual cloud volumes to system database
    # todo: Setup rainclouds_server API

    print("[Build Cloud System] The build of the rainclouds filesystem structure has completed successfully!")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_cloud_system(load_toolchain(_toolchain_file))
