import sys
import threading
import time

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain


# Buildstep functions
from pipelines.system.buildsteps.sources.delete_source_dir import delete_source_dir
from pipelines.system.buildsteps.sources.pull_source import pull_source


def pull_sources(toolchain):
    print("[Pull Sources] Pulling dependencies from source repositories...")
    delete_source_dir(toolchain['root'])

    threads = []
    for dep, source in toolchain['sources'].items():
        threads.append(threading.Thread(target=pull_source, args=(toolchain['root'], dep, source)))

    current_index = 0
    thread_count = toolchain['source-pull-threads']
    print("[Pull Sources] Running source pulls in parallel with {0} threads.".format(str(thread_count)))
    while current_index < len(threads):
        remaining_threads = len(threads) - current_index
        final_index = min([current_index + thread_count, current_index + remaining_threads])
        for i in range(current_index, final_index):
            threads[i].start()
        for i in range(current_index, final_index):
            threads[i].join()
        current_index = final_index
    print("[Prepare Sources] System toolchain sources have been successfully pulled...")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    pull_sources(load_toolchain(_toolchain_file))
