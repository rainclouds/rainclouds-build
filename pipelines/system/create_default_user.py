import os
import sys
import shutil
import subprocess

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain, run_within


def update_home_permissions(toolchain):
    print("[Create User] Updating permissions for the Home directory of the new user {0}".format(toolchain['user_home']))
    print("{0}/builds/sources".format(toolchain['local_user_home']))
    if not os.path.isdir("{0}/builds/sources".format(toolchain['local_user_home'])):
        os.makedirs("{0}/builds/sources".format(toolchain['local_user_home']))
    subprocess.call(["chown", "-Rv", "1000:999", toolchain['local_user_home']])
    subprocess.call(["ls", "-la", toolchain['local_user_home']])


def create_default_user(toolchain):
    print("[Create User] Creating default user, rainclouds...")

    print("[Create User] Copying conf/etc/group to {0}/etc/group".format(toolchain['root']))
    shutil.copyfile("conf/etc/group", "{0}/etc/group".format(toolchain['root']))

    print("[Create User] Copying conf/etc/passwd to {0}/etc/passwd".format(toolchain['root']))
    shutil.copyfile("conf/etc/passwd", "{0}/etc/passwd".format(toolchain['root']))

    run_within(toolchain, update_home_permissions, toolchain['root'], 0, 0)
    print("[Create User] Creation of default user, rainclouds, has completed successfully...")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    create_default_user(load_toolchain(_toolchain_file))
