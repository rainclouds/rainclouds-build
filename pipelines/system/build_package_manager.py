import sys

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain

# Package Manager Build Steps
from pipelines.system.buildsteps.pacman.build_zlib import build_zlib
from pipelines.system.buildsteps.pacman.build_libarchive import build_libarchive
from pipelines.system.buildsteps.pacman.build_pkgconfig import build_pkgconfig
from pipelines.system.buildsteps.pacman.build_libcap import build_libcap
from pipelines.system.buildsteps.pacman.build_fakeroot import build_fakeroot
from pipelines.system.buildsteps.pacman.build_openssl import build_openssl
from pipelines.system.buildsteps.pacman.build_pacman import build_pacman, install_pacman


def build_package_manager(toolchain):
    build_zlib(toolchain)
    build_libarchive(toolchain)
    build_pkgconfig(toolchain)
    build_libcap(toolchain)
    build_fakeroot(toolchain)
    build_openssl(toolchain)
    build_pacman(toolchain)


def install_package_manager(toolchain):
    print("[Install Package Manager] Installing package manager...")
    install_pacman(toolchain)
    print("[Install Package Manager] Completed the installation of your package manager.")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_package_manager(load_toolchain(_toolchain_file))
