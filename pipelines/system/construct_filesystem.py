import sys

# Pipeline Builsteps
from pipelines.system.buildsteps.filesystem.make_directories import make_directories
from pipelines.system.buildsteps.filesystem.remove_existing_fs import remove_existing_fs
from pipelines.system.buildsteps.filesystem.link_directories import link_directories

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain


def construct_filesystem(toolchain):
    print("[Prepare FS] Instantiating Linux FileSystem Structure")
    remove_existing_fs(toolchain['root'])
    make_directories(toolchain['root'], toolchain['target'])
    link_directories(toolchain['root'], toolchain['target'])
    print("[Prepare FS] Completed the construction of the base Linux FileSystem Structure.")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    construct_filesystem(load_toolchain(_toolchain_file))
