import sys
from pipelines.system.util import load_toolchain


def xcompile_temp_tools(toolchain):
    print("[Cross Compile Temporary Tools] Cross Compiling of the Temporary System Building Toolkit from sources...")
    print("[Cross Compile Temporary Tools] Completed the cross compilation of Temporary System Building Tools...")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    xcompile_temp_tools(load_toolchain(_toolchain_file))
