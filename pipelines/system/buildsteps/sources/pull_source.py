import glob
import subprocess
import os
import shutil


def format_source(source):
    out = source.copy()
    if 'filename' in out.keys():
        out['filename'] = out['filename'].format(version=out['version'])
    if 'release-tag' in out.keys():
        out['release-tag'] = out['release-tag'].format(version=out['version'])
    if 'repository' in out.keys():
        filename = out['filename'] if 'filename' in out.keys() else None
        out['repository'] = out['repository'].format(version=out['version'], filename=filename)
    if 'moves' in out.keys():
        for i in range(0, len(out['moves'])):
            out['moves'][i][0] = out['moves'][i][0].format(version=out['version'])
            out['moves'][i][1] = out['moves'][i][1].format(version=out['version'])
    return out


def pull_source(root_dir, dep, source):
    source = format_source(source)
    source_dir = root_dir + "/sources/" + dep
    if source['vcs-engine'] == 'git':
        print("[Pull Source] Git - Cloning repository {0} - tag {1}".format(source['repository'], source['release-tag']))
        subprocess.call([
            "git",
            "clone",
            "--depth",
            "1",
            "--branch",
            source['release-tag'],
            source['repository'],
            "{0}/sources/{1}".format(root_dir, dep),
        ], stdout=subprocess.DEVNULL)
        print("[Pull Source] Git - Completed clone {0} - tag {1}".format(source['repository'], source['release-tag']))
    elif source['vcs-engine'] == 'mercurial':
        print("[Pull Source] Mercurial - Cloning repository {0}".format(source['repository']))
        subprocess.call([
            'hg',
            'clone',
            source['repository'],
            source_dir
        ], stdout=subprocess.DEVNULL)
        print("[Pull Source] Mercurial - Completed clone {0}".format(source['repository']))
    elif source['vcs-engine'] == 'tar':
        print("[Pull Source] Tar - Download tar file {0}".format(source['repository']))
        subprocess.call([
            'wget',
            '--quiet',
            '-O',
            "{0}/sources/{1}".format(root_dir, source['filename']),
            source['repository']
        ])
        print("[Pull Source] Tar - Extracting tar file {0}/sources/{1}".format(root_dir, source['filename']))
        tar_flags = "-xzf"
        if ".tar.gz" in source['filename']:
            tar_flags = "-xzf"
        elif ".tar.xz" in source['filename']:
            tar_flags = "-xf"
        subprocess.call([
            'tar', tar_flags, "{0}/sources/{1}".format(root_dir, source['filename']), "-C", "{0}/sources/".format(root_dir)
        ], stdout=subprocess.DEVNULL)
        subprocess.call([
            'rm', "{0}/sources/{1}".format(root_dir, source['filename'])
        ])
        print("[Pull Source] Tar - Completed download and extractions, {0}".format(source['repository']))
    else:
        raise "Unsupported vcs-engine defined for source library, {0}".format(dep)

    if "moves" in source.keys():
        for move in source['moves']:
            print("[Pull Source] Moving Directory, '{0}/sources/{1}' to '{0}/sources/{2}'".format(root_dir, move[0], move[1]))
            files = glob.glob("{0}/sources/{1}".format(root_dir, move[0]))
            if len(files) > 1:
                raise Exception("Too many files to move after tar extraction in a single query, expected 1 result per regex!")
            for file in files:
                shutil.move(file, "{0}/sources/{1}".format(root_dir, move[1]))

    if "patches" in source.keys():
        os.makedirs("{0}/sources/patches/{1}".format(root_dir, dep))
        for patch_name, patch_url in source['patches'].items():
            print("[Pull Source] Downloading patch {0}/sources/patches/{1}/{2}".format(root_dir, dep, patch_name))
            subprocess.call([
                "wget",
                '--quiet',
                "-O",
                "{0}/sources/patches/{1}/{2}".format(root_dir, dep, patch_name),
                patch_url
            ])
            print("[Pull Source] Patch {0}/sources/patches/{1}/{2} has been successfully saved to the local filesystem...".format(root_dir, dep, patch_name))
