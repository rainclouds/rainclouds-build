import os
import shutil


def delete_source_dir(root_dir):
    if os.path.exists("{0}/sources".format(root_dir)):
        print("[Prepare Sources] Cleaning existing sources workspace...")
        shutil.rmtree("{0}/sources".format(root_dir))
