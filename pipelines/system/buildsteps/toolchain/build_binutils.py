import sys
import os
import subprocess
from pipelines.system.util import load_toolchain


def build_binutils(toolchain):
    print("[Build Toolchain] Building BinUtils...")
    os.makedirs("{0}/sources/binutils/build".format(toolchain['root']))
    subprocess.call([
        "sh",
        "../configure",
        "--prefix=/tools",
        "--with-sysroot={0}".format(toolchain['root']),
        "--target={0}".format(toolchain['target']),
        "--disable-nls",
        "--enable-gprofng=no",
        "--disable-werror"
    ], cwd="{0}/sources/binutils/build".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        *toolchain['make-flags']
    ], cwd="{0}/sources/binutils/build".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        "install"
    ], cwd="{0}/sources/binutils/build".format(toolchain['root']), stdout=toolchain['pipe'])
    print("[Build Toolchain] Completed building BinUtils.")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_binutils(load_toolchain(_toolchain_file))
