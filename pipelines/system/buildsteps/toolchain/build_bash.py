import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_bash(toolchain):
    print("[Build TempTools] Beginning compilation of Coreutils...")
    guess = subprocess.check_output([
        'sh', 'support/config.guess'
    ], cwd="{0}/sources/bash".format(toolchain['root'])).decode(sys.stdout.encoding).strip()
    print("Config Guess: {0}".format(guess))
    subprocess.call([
        "./configure",
        "--prefix=/usr",
        "--host={0}".format(toolchain['target']),
        "--build={0}".format(guess),
        "--without-bash-malloc"
    ], cwd="{0}/sources/bash".format(toolchain['root']))
    subprocess.call(["make"], cwd="{0}/sources/bash".format(toolchain['root']))
    subprocess.call([
        "make",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/bash".format(toolchain['root']))
    subprocess.call([
        "ln", "-sv", "bash", "{0}/bin/sh".format(toolchain['root'])
    ], cwd="{0}/sources/bash".format(toolchain['root']))
    print("[Build TempTools] Bash has been successfully compiled!")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_bash(load_toolchain(_toolchain_file))
