import os
import re
import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_glibc(toolchain):
    print("[Build Toolchain] Compiling Glibc...")
    if re.search("i.86", toolchain['target']):
        subprocess.call([
            "ln",
            "-sfv",
            "../lib/ld-linux-x86-64.so.2",
            "{0}/lib/ld-lsb.so.3".format(toolchain['root'])
        ], cwd="{0}/sources/glibc".format(toolchain['root']))
    elif "x86_64" in toolchain['target']:
        subprocess.call([
            "ln",
            "-sfv",
            "../lib/ld-linux-x86-64.so.2",
            "{0}/lib64".format(toolchain['root'])
        ], cwd="{0}/sources/glibc".format(toolchain['root']))
        subprocess.call([
            "ln",
            "-sfv",
            "../lib/ld-linux-x86-64.so.2",
            "{0}/lib64/ld-lsb-x86-64.so.3".format(toolchain['root'])
        ], cwd="{0}/sources/glibc".format(toolchain['root']))
    subprocess.call([
        "patch",
        "-Np1",
        "-i",
        "{0}/sources/patches/glibc/glibc-2.38-fhs-1.patch".format(toolchain['root'])
    ], cwd="{0}/sources/glibc".format(toolchain['root']))
    
    os.makedirs("{0}/sources/glibc/build".format(toolchain['root']))
    with open("{0}/tmp/guessed_config".format(toolchain['root']), "w") as f:
        subprocess.call([
            "sh",
            "../scripts/config.guess"
        ], cwd="{0}/sources/glibc/build".format(toolchain['root']), stdout=f)
        subprocess.call([
            "echo",
            "rootsbindir=/usr/sbin",
            ">",
            "configparms"
        ], cwd="{0}/sources/glibc/build".format(toolchain['root']))

    with open("{0}/tmp/guessed_config".format(toolchain['root']), "r") as f:
        guessed = f.read()
        print(guessed)
        subprocess.call([
            "../configure",
            "--prefix=/usr",
            "--host={0}".format(toolchain['target']),
            "--build={0}".format(guessed),
            "--enable-kernel=4.14",
            "--with-headers={0}/usr/include".format(toolchain['root']),
            "libc_cv_slibdir=/usr/lib"
        ], cwd="{0}/sources/glibc/build".format(toolchain['root']))
        subprocess.call([
            "make",
            "-j1"
        ], cwd="{0}/sources/glibc/build".format(toolchain['root']))
        subprocess.call([
            "make",
            "DESTDIR={0}".format(toolchain['root']),
            "install"
        ], cwd="{0}/sources/glibc/build".format(toolchain['root']))
        subprocess.call([
            "sed",
            "/RTLDLIST=/s@/usr@@g",
            "-i",
            "{0}/usr/bin/ldd".format(toolchain['root'])
        ],cwd="{0}/sources/glibc/build".format(toolchain['root']))
    print("[Build Toolchain] GLibc has been successfully compiled and installed.")

if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_glibc(load_toolchain(_toolchain_file))
