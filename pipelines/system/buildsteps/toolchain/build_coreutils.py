import shutil
import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_coreutils(toolchain):
    print("[Build TempTools] Beginning compilation of Coreutils...")
    guess = subprocess.check_output([
        './build-aux/config.guess'
    ], cwd="{0}/sources/coreutils".format(toolchain['root'])).decode(sys.stdout.encoding).strip()
    print("Config Guess: {0}".format(guess))
    subprocess.call([
        "./configure",
        "--prefix=/usr",
        "--host={0}".format(toolchain['target']),
        "--build={0}".format(guess),
        "--enable-install-program=hostname",
        "--enable-no-install-program=kill,uptime",
        "gl_cv_macro_MB_CUR_MAX_good=y"
    ], cwd="{0}/sources/coreutils".format(toolchain['root']))
    subprocess.call(["make"], cwd="{0}/sources/coreutils".format(toolchain['root']))
    subprocess.call([
        "make",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/coreutils".format(toolchain['root']))
    subprocess.call([
        "mv", "-v",
        "{0}/usr/bin/chroot".format(toolchain['root']),
        "{0}/usr/sbin".format(toolchain['root'])])
    subprocess.call([
        "mkdir", "-pv", "{0}/usr/share/man/man8".format(toolchain['root'])
    ])
    subprocess.call([
        "mv", "-v",
        "{0}/usr/share/man/man1/chroot.1".format(toolchain['root']),
        "{0}/usr/share/man/man8/chroot.8".format(toolchain['root'])
    ])
    subprocess.call([
        "sed", "-i",
        "'s/\"1\"/\"8\"/'".format(toolchain['root']),
        "{0}/usr/share/man/man8/chroot.8".format(toolchain['root'])
    ], cwd="{0}/sources/bash".format(toolchain['root']))
    print("[Build TempTools] Coreutils has been successfully compiled!")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_coreutils(load_toolchain(_toolchain_file))
