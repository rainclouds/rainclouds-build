import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_linux_api_headers(toolchain):
    print("[Build Toolchain] Compiling Linux api headers...")
    subprocess.call([
        "make",
        "mrpoper"
    ], cwd="{0}/sources/linux".format(toolchain['root']))
    subprocess.call([
        "make",
        "headers"
    ], cwd="{0}/sources/linux".format(toolchain['root']))
    subprocess.call([
        "find",
        "usr/include",
        "-type",
        "f",
        "!",
        "-name",
        "*.h",
        "-delete"
    ], cwd="{0}/sources/linux".format(toolchain['root']))
    subprocess.call([
        "cp",
        "-rv",
        "usr/include",
        "{0}/usr".format(toolchain['root'])
    ], cwd="{0}/sources/linux".format(toolchain['root']))
    print("[Build Toolchain] Linux api headers have been succesfully compiled and installed.")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_linux_api_headers(load_toolchain(_toolchain_file))
    