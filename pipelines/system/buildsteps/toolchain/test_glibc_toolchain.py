import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def test_glibc_toolchain(toolchain):
    print("[Test GlibC] Testing the build of glibc")
    out_proc = subprocess.Popen(['echo', 'int main(){}'],
                                cwd="{0}/sources/glibc/build".format(toolchain['root']),
                                stdout=subprocess.PIPE)
    in_proc = subprocess.Popen(['{0}-gcc'.format(toolchain['target']), '-xc', '-'],
                               cwd="{0}/sources/glibc/build".format(toolchain['root']),
                               stdin=out_proc.stdout,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    out_proc.stdout.close() # Allow proc1 to receive a SIGPIPE if proc2 exits.
    out, err = in_proc.communicate()
    if err:
        raise Exception(err)
    print('{0}-gcc output: {1}'.format(toolchain['target'], out))
    out_proc = subprocess.Popen(['readelf', '-l', 'a.out'],
                                cwd="{0}/sources/glibc/build".format(toolchain['root']),
                                stdout=subprocess.PIPE)
    in_proc = subprocess.Popen(['grep', 'ld-linux'],
                               cwd="{0}/sources/glibc/build".format(toolchain['root']),
                               stdin=out_proc.stdout,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    out, err = in_proc.communicate()
    if err:
        raise Exception(err)
    print('readelf output: {0}'.format(out))
    subprocess.call(["rm", "-v", "a.out"], cwd="{0}/sources/glibc/build".format(toolchain['root']))
    print("[Test GlibC] Completed the test of GLibC")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    test_glibc_toolchain(load_toolchain(_toolchain_file))