import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_libstdcpp(toolchain):
    print("[Build Toolchain] Compiling libstdc++...")
    with open("{0}/tmp/guessed_config".format(toolchain['root']), "w") as f:
        subprocess.call(["sh", "../scripts/config.guess"], cwd="{0}/sources/glibc/build".format(toolchain['root']), stdout=f)
    with open("{0}/tmp/guessed_config".format(toolchain['root']), "r") as f:
        guessed = f.read()
        subprocess.call(["../libstdc++-v3/configure",
                         "--host={0}".format(toolchain['target']),
                         "--build={0}".format(guessed),
                         "--prefix=/usr",
                         "--disable-multilib",
                         "--disable-nls",
                         "--disable-libstdcxx-pch",
                         "--with-gxx-include-dir=/tools/{0}/include/c++/{1}".format(toolchain['target'], toolchain['sources']['gcc']['version'])],
                        cwd="{0}/sources/gcc/build".format(toolchain['root']))
        subprocess.call(["make", *toolchain['make-flags']], cwd="{0}/sources/gcc/build".format(toolchain['root']))
        subprocess.call(["make", "DESTDIR={0}".format(toolchain['root']), "install"], cwd="{0}/sources/gcc/build".format(toolchain['root']))
        subprocess.call(["rm", "-v", "{0}/usr/lib/libstdc++.la".format(toolchain['root'])], cwd="{0}/sources/gcc/build".format(toolchain['root']))
        subprocess.call(["rm", "-v", "{0}/usr/lib/libstdc++fs.la".format(toolchain['root'])], cwd="{0}/sources/gcc/build".format(toolchain['root']))
        subprocess.call(["rm", "-v", "{0}/usr/lib/libsupc++.la".format(toolchain['root'])], cwd="{0}/sources/gcc/build".format(toolchain['root']))
        print("[Build Toolchain] Compilation of libstdc++ has completed successfully!")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_libstdcpp(load_toolchain(_toolchain_file))
