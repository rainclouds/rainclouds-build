import sys
import os
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_gcc(toolchain):
    print("[Build Toolchain] Building GCC...")
    subprocess.call(['ln', '-sv', '../mpfr', './mpfr'], cwd="{0}/sources/gcc".format(toolchain['root']))
    subprocess.call(['ln', '-sv', '../gmp', './gmp'], cwd="{0}/sources/gcc".format(toolchain['root']))
    subprocess.call(['ln', '-sv', '../mpc', './mpc'], cwd="{0}/sources/gcc".format(toolchain['root']))

    # On x86_64 hosts, set the default directory name for 64-bit libraries to “lib”
    if "x86_64" in toolchain['target']:
        subprocess.call(['sed',
                         '-e',
                         '/m64=/s/lib64/lib/',
                         '-i.orig',
                         'gcc/config/i386/t-linux64'],
                        cwd="{0}/sources/gcc".format(toolchain['root']))
    os.makedirs("{0}/sources/gcc/build".format(toolchain['root']))
    subprocess.call([
        "sh",
        "../configure",
        "--prefix=/tools",
        "--with-sysroot={0}".format(toolchain['root']),
        "--target={0}".format(toolchain['target']),
        "--with-glibc-version={0}".format(toolchain['sources']['glibc']['version']),
        "--with-newlib",
        "--without-headers",
        "--enable-default-pie",
        "--enable-default-ssp",
        "--disable-nls",
        "--disable-shared",
        "--disable-multilib",
        "--disable-threads",
        "--disable-libatomic",
        "--disable-libgomp",
        "--disable-libquadmath",
        "--disable-libssp",
        "--disable-libvtv",
        "--disable-libstdcxx",
        "--enable-languages=c,c++"
    ], cwd="{0}/sources/gcc/build".format(toolchain['root']))
    subprocess.call(["make", *toolchain['make-flags']], cwd="{0}/sources/gcc/build".format(toolchain['root']))
    subprocess.call(["make", "install"], cwd="{0}/sources/gcc/build".format(toolchain['root']))
    gcc_out = subprocess.check_output(["{0}".format(toolchain['target'] + "-gcc"), "-print-libgcc-file-name"])
    dirname = subprocess.check_output(["dirname", "{0}".format(gcc_out.decode(sys.stdout.encoding).strip())])

    # Create a full limits.h file by joining limitx.h, glimits.h, and limity.h.
    print("Creating limits.h at: {0}/include/limits.h".format(dirname.decode(sys.stdout.encoding).strip()))
    output = ""
    for filename in ['gcc/limitx.h', 'gcc/glimits.h', 'gcc/limity.h']:
        with open("{0}/sources/gcc/{1}".format(toolchain['root'], filename), "r") as f:
            output += f.read() + "\n"
    with open("{0}/include/limits.h".format(dirname.decode(sys.stdout.encoding).strip()), "w") as limits_h:
        print("Writing compiled limits header to {0}/include/limits.h.".format(dirname.decode(sys.stdout.encoding).strip()))
        limits_h.write(output)
    print("[Build Toolchain] Completed building GCC")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_gcc(load_toolchain(_toolchain_file))
