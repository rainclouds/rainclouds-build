import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_m4(toolchain):
    print("[Build TempTools] Beginning compilation of TempTools...")
    # subprocess.call(["./bootstrap"], cwd="{0}/sources/m4".format(toolchain['root']))
    guess = subprocess.check_output([
        './build-aux/config.guess'
    ], cwd="{0}/sources/m4".format(toolchain['root'])).decode(sys.stdout.encoding).strip()
    print("Config Guess: {0}".format(guess))
    subprocess.call([
        "./configure",
        "--prefix=/usr",
        "--host={0}".format(toolchain['target']),
        "--build={0}".format(guess)
    ], cwd="{0}/sources/m4".format(toolchain['root']))
    subprocess.call(["make"], cwd="{0}/sources/m4".format(toolchain['root']))
    # subprocess.call([
    #     "make",
    #     "check"
    # ], cwd="{0}/sources/m4".format(toolchain['root']))
    subprocess.call([
        "make",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/m4".format(toolchain['root']))
    print("[Build TempTools] M4 has been successfully compiled!")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_m4(load_toolchain(_toolchain_file))
