import subprocess
import os


def build_ncurses(root_dir, target, make_flags):
    print("[Build TempTools] Compiling NCurses...")
    subprocess.call(["sed", "-i", "s/mawk// configure"], cwd="{0}/sources/ncurses".format(root_dir))
    os.makedirs("{0}/sources/ncurses/build".format(root_dir))
    subprocess.call(["../configure"], cwd="{0}/sources/ncurses/build".format(root_dir))
    subprocess.call(["make", "-C", "include"], cwd="{0}/sources/ncurses/build".format(root_dir))
    subprocess.call(["make", "-C", "progs", "tic"], cwd="{0}/sources/ncurses/build".format(root_dir))
    subprocess.call(["./configure",
                     "--prefix=/usr",
                     "--host={0}".format(target),
                     "--build=$(./config.guess)",
                     "--mandir=/usr/share/man",
                     "--with-manpage-format=normal",
                     "--with-shared",
                     "--without-normal",
                     "--with-cxx-shared",
                     "--without-debug",
                     "--without-ada",
                     "--disable-stripping",
                     "--enable-widec"],
                    cwd="{0}/sources/ncurses".format(root_dir))
    subprocess.call(["make", make_flags], cwd="{0}/sources/ncurses".format(root_dir))
    subprocess.call(["make",
                     "DESTDIR={0}".format(root_dir),
                     "TIC_PATH=$(pwd)/build/progs/tic",
                     "install"],
                    cwd="{0}/sources/ncurses".format(root_dir))
    subprocess.call(["echo", "INPUT(-lncursesw)", ">", "{0}/usr/lib/libncurses.so".format(root_dir)], cwd="{0}/sources/ncurses".format(root_dir))
    print("[Build TempTools] Compilation of NCurses have completed successfully")
