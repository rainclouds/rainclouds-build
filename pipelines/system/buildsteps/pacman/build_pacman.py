import os
import sys
import shutil
import subprocess

# Pipeline Utility Function
from pipelines.system.buildsteps.sources.pull_source import format_source
from pipelines.system.util import load_toolchain, run_within, copy_dir


def build_pacman(toolchain):
    print("[Build Package Manager] Compiling Pacman package manager...")
    subprocess.call([
        "meson",
        "build",
        "--prefix", "/tools",
        "--default-library", "static",
        "--sysconfdir", "/etc",
        "--localstatedir", "/var"
    ], cwd="{0}/sources/pacman".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "ninja", "-C", "build"
    ], cwd="{0}/sources/pacman".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "ninja", "-C", "build", "install"
    ], cwd="{0}/sources/pacman".format(toolchain['root']), stdout=toolchain['pipe'])

    print("[Build Package Manager] Copying conf/etc/pacman.conf to {0}/etc/pacman.conf".format(toolchain['root']))
    shutil.copyfile("conf/etc/pacman.conf", "{0}/etc/pacman.conf".format(toolchain['root']))
    shutil.copyfile("conf/etc/makepkg.conf", "{0}/etc/makepkg.conf".format(toolchain['root']))
    if not os.path.exists("{0}/etc/pacman.d".format(toolchain['root'])):
        os.makedirs("{0}/etc/pacman.d".format(toolchain['root']))
    shutil.copyfile("conf/etc/pacman.d/mirrorlist", "{0}/etc/pacman.d/mirrorlist".format(toolchain['root']))
    print("[Build Package Manager] Compilation of pacman has completed successfully!")


# Used to create a package of pacman with to install pacman, using pacman.
def install_pacman(toolchain):
    print("[Package Package Manager] Packaging pacman for installation...")
    copy_dir("./conf/pacman/PKGBUILD", "{0}/sources/pacman/PKGBUILD".format(toolchain['root']))
    run_within(toolchain, __make_pkg, toolchain['root'], 1000, 999, {
        'PATH': '/bin:/usr/bin:/sbin:/usr/sbin:/tools/bin:/tools/sbin:/tools/lib',
        'PS1': '$? \\u:\w \$ ',
        'HOME': toolchain['local_user_home']
    })
    print("[Package Package Manager] Pacman has been successfully packaged and is ready for installation!")


# Runs under a chroot umbrella
def __make_pkg(toolchain):
    print("[Package Package Manager] Installing pacman....")
    version = toolchain['sources']['pacman']['version']
    subprocess.call(["makepkg", "--skipchecksums"], cwd="/sources/pacman")
    subprocess.call([
        "pacman", "-U", "pacman-{0}-{1}.pkg.tar.gz".format(version, toolchain['architecture'])
    ], cwd="/sources/pacman")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_pacman(load_toolchain(_toolchain_file))
