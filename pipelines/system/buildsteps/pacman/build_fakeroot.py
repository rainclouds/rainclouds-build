import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_fakeroot(toolchain):
    print("[Build Toolchain] Compiling fakeroot...")
    subprocess.call([
        "./configure",
        "--prefix=/tools",
        "--libdir=/tools/lib/libfakeroot",
        "--with-ipc=sysv"
    ], cwd="{0}/sources/fakeroot".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        *toolchain['make-flags']
    ], cwd="{0}/sources/fakeroot".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/fakeroot".format(toolchain['root']), stdout=toolchain['pipe'])
    print("[Build Toolchain] Compilation of fakeroot has completed successfully!")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_fakeroot(load_toolchain(_toolchain_file))
