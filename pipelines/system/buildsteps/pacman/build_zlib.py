import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_zlib(toolchain):
    print("[Build Toolchain] Compiling zlib...")
    subprocess.call([
        "./configure",
        "--prefix=/tools"
    ], cwd="{0}/sources/zlib".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        *toolchain['make-flags']
    ], cwd="{0}/sources/zlib".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/zlib".format(toolchain['root']), stdout=toolchain['pipe'])
    print("[Build Toolchain] Compilation of zlib has completed successfully!")


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_zlib(load_toolchain(_toolchain_file))
