import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_pkgconfig(toolchain):
    print("[Build Toolchain] Compiling pkg-config...")
    # subprocess.call([
    #     "sh",
    #     "autogen.sh"
    # ], cwd="{0}/sources/pkg-config".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "./configure",
        "--prefix=/tools",
        "--with-internal-glib",
        "--disable-compile-warnings",
        "--disable-host-tool",
        "--disable-shared",
        "--docdir=/tools/share/doc/pkg-config-{0}".format(toolchain['sources']['pkg-config']['version'])
    ], cwd="{0}/sources/pkg-config".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        *toolchain['make-flags']
    ], cwd="{0}/sources/pkg-config".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/pkg-config".format(toolchain['root']), stdout=toolchain['pipe'])
    print("[Build Toolchain] Compilation of pkg-config has completed successfully!")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_pkgconfig(load_toolchain(_toolchain_file))
