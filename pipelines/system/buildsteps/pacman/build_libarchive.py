import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_libarchive(toolchain):
    print("[Build Toolchain] Compiling libarchive...")
    subprocess.call([
        "sh",
        "build/autogen.sh"
    ], cwd="{0}/sources/libarchive".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "./configure",
        "--prefix=/tools",
        "--without-xml2"
    ], cwd="{0}/sources/libarchive".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        *toolchain['make-flags']
    ], cwd="{0}/sources/libarchive".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/libarchive".format(toolchain['root']), stdout=toolchain['pipe'])
    print("[Build Toolchain] Compilation of libarchive has completed successfully!")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_libarchive(load_toolchain(_toolchain_file))
