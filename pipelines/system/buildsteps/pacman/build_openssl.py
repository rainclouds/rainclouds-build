import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain, run_within


def build_openssl(toolchain):
    run_within(toolchain, __build_openssl, toolchain['root'], 0, 0, {
        "HOME": "/root",
        "PATH": "/usr/bin:/usr/sbin"
    })


def __build_openssl(toolchain):
    print("[Build Toolchain] Compiling openssl...")
    subprocess.call([
        "./config", "--prefix=/usr",
        "--openssldir=/etc/ssl",
        "--libdir=lib",
        "shared",
        "zlib-dynamic",
    ], cwd="/sources/openssl")
    subprocess.call([
        "make",
        *toolchain['make-flags']
    ], cwd="/sources/openssl")
    subprocess.call([
        "sed", "-i", "'/INSTALL_LIBS/s/libcrypto.a libssl.a//'", "Makefile"
    ], cwd="/sources/openssl")
    subprocess.call([
        "mv", "-v", "/usr/share/doc/openssl", "/usr/share/doc/openssl-3.2.0"
    ], cwd="/sources/openssl")
    subprocess.call(["make", "MANSUFFIX=ssl", "install"], cwd="/sources/openssl")
    subprocess.call([
        "cp", "-vfr", "doc/*", "/usr/share/doc/openssl-3.2.0"
    ], cwd="/sources/openssl")
    print("[Build Toolchain] Compilation of openssl has completed successfully!")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_openssl(load_toolchain(_toolchain_file))
