import sys
import subprocess

# Pipeline Utility Function
from pipelines.system.util import load_toolchain


def build_libcap(toolchain):
    print("[Build Toolchain] Compiling libcap2...")
    subprocess.call([
        "make",
        *toolchain['make-flags']
    ], cwd="{0}/sources/libcap2".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "make",
        "RAISE_SETFCAP=no",
        "lib=lib",
        "prefix=/tools",
        "DESTDIR={0}".format(toolchain['root']),
        "install"
    ], cwd="{0}/sources/libcap2".format(toolchain['root']), stdout=toolchain['pipe'])
    subprocess.call([
        "chmod",
        "-v",
        "755",
        "{0}/tools/lib/libcap.so".format(toolchain['root'])
    ], cwd="{0}/sources/libcap2".format(toolchain['root']), stdout=toolchain['pipe'])
    print("[Build Toolchain] Compilation of libcap2 has completed successfully!")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_libcap(load_toolchain(_toolchain_file))
