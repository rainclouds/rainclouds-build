import os


def make_directories(root_dir, target):
    print("[Prepare FS] Constructing filesystem directories")
    root_dirs = ["etc", "var", "tmp", "opt", "sources/patches", "tools", "boot/efi", "home"]
    usr_dirs = ["bin", "lib", "sbin", "src"]

    for d in root_dirs:
        os.makedirs("{0}/{1}".format(root_dir, d))
    for d in usr_dirs:
        os.makedirs("{0}/usr/{1}".format(root_dir, d))
    if "x86_64" in target:
        os.makedirs("{0}/{1}".format(root_dir, "usr/lib64"))
