import subprocess


def link_directories(root_dir, target):
    print("[Prepare FS] Creating symbolic links within the newly instantiated linux filesystem...")
    linked_dirs = {"usr/bin": "bin", "usr/sbin": "sbin", "usr/lib": "lib"}
    for dst, src in linked_dirs.items():
        subprocess.call(["ln", "-sv", dst, src], cwd=root_dir)
    if "x86_64" in target:
        subprocess.call(["ln", "-sv", "usr/lib64", "lib64"], cwd=root_dir)
