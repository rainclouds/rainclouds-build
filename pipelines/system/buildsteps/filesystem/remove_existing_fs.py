import os
import shutil


def remove_existing_fs(root_dir):
    if os.path.exists(root_dir):
        print("[Prepare FS] Cleaning existing build output filesystem.")
        shutil.rmtree(root_dir)
