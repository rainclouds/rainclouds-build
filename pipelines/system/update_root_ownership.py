import os
import sys
import shutil

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain


def update_root_ownership(toolchain):
    print("[Update Ownership] Setting ownership of root directories to root user...")
    for p in ["usr", "lib", "var", "etc", "bin", "sbin", "tools"]:
        os.system("chown -R root:root {0}/{1}".format(toolchain['root'], p))
        print("Granted root:root ownership of {0}/{1}".format(toolchain['root'], p))
    if toolchain['architecture'] == "x86_64":
        os.system("chown -R root:root {0}/lib64".format(toolchain['root']))
        print("Granted root:root ownership of {0}/lib64".format(toolchain['root'], p))
    print("[Update Ownership] Successfully set the ownership of root directories to root user.")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    update_root_ownership(load_toolchain(_toolchain_file))
