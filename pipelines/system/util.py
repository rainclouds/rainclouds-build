import json
import os
import subprocess
import shutil, errno


def copy_dir(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:
        if exc.errno in (errno.ENOTDIR, errno.EINVAL):
            shutil.copy(src, dst)
        else:
            raise


def run_within(toolchain, pipeline_function, chroot, user_id=0, group_id=0, env={}):
    print("[CHROOT - FORK] Spinning off a new process fork to run a virtual environment")
    pid = os.fork()
    if pid == 0:
        for key in env.keys():
            os.environ[key] = env[key]
        os.chroot(chroot)
        os.setgid(group_id)
        os.setuid(user_id)
        os.chdir("/")
        pipeline_function(toolchain)
        print("[CHROOT - FORK] Closing process fork for self-contained virtual environment")
        exit(0)
    else:
        os.waitpid(pid, 0)


def download_file(url, filename):
    if os.path.isfile(filename):
        subprocess.call(["rm", filename])
    if filename:
        subprocess.call(['wget', '-O', filename, url])


def extract_tar(tar, extract_path, remove_tar=False):
    print("[Extract Tar] Extracting tar: {0} to {1}".format(tar, extract_path))
    if ".tar.gz" in tar:
        subprocess.call(['tar', "-xzf", tar, "-C", extract_path])
    elif ".tar.xz" in tar:
        subprocess.call(['tar', "-xf", tar, "-C", extract_path])
    else:
        raise Exception("Unsupported file type.")
    print("[Extract Tar] Tar successfully extracted tar: {0} to {1}".format(tar, extract_path))
    if remove_tar:
        subprocess.call(['rm', tar])
        print("[Extract Tar] Tar deleted: {0}".format(tar))


def create_target(arch="x86_64", vendor="rainclouds", _os="linux", clib="gnu"):
    return "{0}-{1}-{2}-{3}".format(arch, vendor, _os, clib)


def load_toolchain(filename):
    with open(filename, 'r') as f:
        print("Toolchain Configuration: {0}".format(filename))
        toolchain = json.load(f)

        os.environ['ROOT_DIR'] = "{0}/build/{1}/system".format(os.getcwd(), toolchain['version'])
        os.environ['REPO_DIR'] = "{0}/build/{1}/repository".format(os.getcwd(), toolchain['version'])
        toolchain['root'] = os.environ['ROOT_DIR']
        toolchain['repo'] = os.environ['REPO_DIR']
        toolchain['user_home'] = "{0}/home/{1}".format(toolchain['root'], toolchain['user'])
        toolchain['local_user_home'] = "/home/{0}".format(toolchain['user'])
        os.environ['DESTDIR'] = os.environ['ROOT_DIR']

        os.environ['TARGET_PLATFORM'] = create_target(
            arch=toolchain['architecture'],
            vendor=toolchain['vendor'])
        toolchain['target'] = os.environ['TARGET_PLATFORM']

        os.environ['LC_ALL'] = 'POSIX'
        os.environ['CONFIG_SITE'] = "{0}/usr/share/config.site".format(os.environ['ROOT_DIR'])

        os.environ['PATH'] += ":{0}/tools/bin".format(os.environ['ROOT_DIR'])
        toolchain['PATH'] = os.environ['PATH']
        toolchain['pipe'] = subprocess.PIPE if 'verbose' in toolchain.keys() and toolchain['verbose'] else subprocess.DEVNULL

        print("Root Directory: {0}".format(os.environ['ROOT_DIR']))
        print("Target Platform: {0}".format(os.environ['TARGET_PLATFORM']))
        print("Config Sit: {0}".format(os.environ['CONFIG_SITE']))
        print("LC_ALL: {0}".format(os.environ['LC_ALL']))
        print("Path Variable: {0}".format(os.environ['PATH']))

        return toolchain
