import os
import sys
import subprocess

from pipelines.system.util import load_toolchain


def prepare_virtual_kernel(toolchain):
    print("[Prepare Kernel] Preparing virtual kernel...")

    # Create directories for virtual filesystems
    for subdir in ["dev", "proc", "sys", "run"]:
        path = "{0}/{1}".format(toolchain['root'], subdir)
        if os.path.isdir(path):
            # Check if the directory is a mount
            if os.path.ismount(path):
                print("[Prepare Kernel] Unmounting existing directory: {0}".format(path))
                subprocess.call(["umount", path])
            subprocess.call(["rm", "-rf", path])
        subprocess.call(["mkdir", "-pv", path])

    subprocess.call(["mount", "-v", "--bind", "/dev", "{0}/dev".format(toolchain['root'])])
    subprocess.call(["mount", "-v", "--bind", "/dev/pts", "{0}/dev/pts".format(toolchain['root'])])
    subprocess.call(["mount", "-vt", "proc", "proc", "{0}/proc".format(toolchain['root'])])
    subprocess.call(["mount", "-vt", "sysfs", "sysfs", "{0}/sys".format(toolchain['root'])])
    subprocess.call(["mount", "-vt", "tmpfs", "tmpfs", "{0}/run".format(toolchain['root'])])
    print("[Prepare Kernel] virtual kernel ready!")



if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    prepare_virtual_kernel(load_toolchain(_toolchain_file))
