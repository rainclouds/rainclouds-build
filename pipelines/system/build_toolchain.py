import sys

# Pipeline Utility Functions
from pipelines.system.util import load_toolchain

# Buildstep functions
from pipelines.system.buildsteps.toolchain.build_binutils import build_binutils
from pipelines.system.buildsteps.toolchain.build_gcc import build_gcc
from pipelines.system.buildsteps.toolchain.build_linux_api_headers import build_linux_api_headers
from pipelines.system.buildsteps.toolchain.build_glibc import build_glibc
from pipelines.system.buildsteps.toolchain.test_glibc_toolchain import test_glibc_toolchain
from pipelines.system.buildsteps.toolchain.build_libstdcpp import build_libstdcpp
from pipelines.system.buildsteps.temptools.build_m4 import build_m4
from pipelines.system.buildsteps.toolchain.build_coreutils import build_coreutils
from pipelines.system.buildsteps.toolchain.build_bash import build_bash


def build_toolchain(toolchain):
    print("[Build Toolchain] Compiling System Tools from sources...")
    build_binutils(toolchain)
    build_gcc(toolchain)
    build_linux_api_headers(toolchain)
    build_glibc(toolchain)
    test_glibc_toolchain(toolchain)
    build_libstdcpp(toolchain)
    build_m4(toolchain)
    build_coreutils(toolchain)
    build_bash(toolchain)
    print("[Build Toolchain] Completed the compilation of System Tools...")


if __name__ == "__main__":
    [_pyfile, _toolchain_file] = sys.argv
    build_toolchain(load_toolchain(_toolchain_file))
