import sys


# Pipeline Buildsteps
from pipelines.system.construct_filesystem import construct_filesystem
from pipelines.system.pull_sources import pull_sources
from pipelines.system.build_toolchain import build_toolchain
from pipelines.system.build_package_manager import build_package_manager, install_package_manager
from pipelines.system.update_root_ownership import update_root_ownership
from pipelines.system.create_default_user import create_default_user
from pipelines.system.install_system_software import install_system_software
from pipelines.system.util import load_toolchain
from pipelines.system.prepare_virtual_kernel import prepare_virtual_kernel
from pipelines.system.build_cloud_system import build_cloud_system


def build_system(toolchain):
    # Prepare Base System
    construct_filesystem(toolchain)
    pull_sources(toolchain)
    build_toolchain(toolchain)
    build_package_manager(toolchain)
    update_root_ownership(toolchain)
    prepare_virtual_kernel(toolchain)
    create_default_user(toolchain)
    install_package_manager(toolchain)
    install_system_software(toolchain)

    # Configure Client Edition Software
    # todo: install rainclouds desktop environment

    # Configure Server Edition Software
    build_cloud_system(toolchain)


if __name__ == '__main__':
    [_pyfile, _toolchain_file] = sys.argv
    build_system(load_toolchain(_toolchain_file))
