import sys

# Pipeline Buildsteps
from pipelines.system.construct_filesystem import construct_filesystem


def build_repository(packages):
    construct_filesystem(packages)


if __name__ == '__main__':
    [_pyfile, _packages_file] = sys.argv
    build_repository(_packages_file)