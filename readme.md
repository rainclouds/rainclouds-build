# Rain Clouds Distribution Builds
The official distribution build system for Rain Cloud Linux. This repository was written following instructions from the [LFS (Linux from Scratch)](https://www.linuxfromscratch.org/) book.

This repository is used to build multiple versions of the Rain Clouds Linux Distribution.
  - Rain Clouds - Server
  - Rain Clouds - Minimal
  - Rain Clouds - Desktop


In this repository you will find build pipelines that do the complete the following tasks:
  - pipelines/build_system.py: Build a self-contained linux distribution
    - Compile all necessary tools up to building a package manager.
    - Configure the package manager to use user-define package repositories to install the remaining tools with.
    - Inject custom installer code to install the compiled linux system on another machine.
    - Generate an ISO which can be formatted to a USB drive that contains the installer for the newly compiled Linux Distribution.
  - pipelines/build_repository.py: Builds officially supported repository packages
  - collectors/pipeline_versions.py: Fatchest latest package versions to generate a configuration to on pipelines/build_system.py.
  - collectors/repository_versions.py: Fetches latest software versions to generate a configuration file to use on the pipelines/build_repository.py.
  
## Building the pipeline configurations
Inside the `collectors` directory, you will find some python scripts for generating the necessary configuration files used to run various build pipelines with. These scripts are more for the developer's convenience. They will visit sites for each package to attempt to determine the latest stable (or nightly, experimental, etc, if specified) version of the given package.

 